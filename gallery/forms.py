from django import forms
from django.forms import Textarea, FileInput
from .models import Image


class ImageForm(forms.ModelForm):
    '''Форма добавления изображения'''

    class Meta:
        model = Image
        fields = ('description', 'img')
        widgets = {
            'description': Textarea(attrs={'class': 'form-input'}),
            'img': FileInput(attrs={'class': 'form-input'}),
        }
