from django.contrib import admin
from .models import Image
from imagekit.admin import AdminThumbnail


class ImageAdmin(admin.ModelAdmin):
    list_display = ('description', 'created', 'approved', 'admin_thumbnail')
    list_filter = ['approved', 'created']
    admin_thumbnail = AdminThumbnail(image_field='img_thumbnail')
    actions = ['on_tag', 'off_tag']

    def on_tag(self, request, queryset):
        '''Включаем отображение на сайте'''

        queryset.update(approved=True)
        self.message_user(request, 'Выбранные объекты опубликованы')

    on_tag.short_description = 'Включить на сайте'

    def off_tag(self, request, queryset):
        '''Выключаем отображение на сайте'''

        queryset.update(approved=False)
        self.message_user(request, 'Выбранные объекты больше не отображаются на сайте')

    off_tag.short_description = 'Выключить на сайте'


admin.site.register(Image, ImageAdmin)
