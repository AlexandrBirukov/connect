from django.urls import path
from .views import (
    MainView,
    ContactView,
    ImageCreateView,
    ImageListView
)

app_name = 'gallery'

urlpatterns = [
    path('', MainView.as_view(), name='main'),
    path('contacts/', ContactView.as_view(), name='contacts'),
    path('load/', ImageCreateView.as_view(), name='load'),
    path('gallery/', ImageListView.as_view(), name='gallery'),
]
