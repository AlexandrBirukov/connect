from django.db import models
from imagekit.models import ImageSpecField
from pilkit.processors import ResizeToFit


class Image(models.Model):
    '''Изображения'''

    description = models.TextField('Комментарий для изображения', blank=True)
    img = models.ImageField('Изображение', upload_to='images')
    img_small = ImageSpecField(
        source='img',
        processors=[ResizeToFit(width=300, height=1000)],
        format='JPEG',
        options={'quality': 80}
    )
    img_thumbnail = ImageSpecField(
        source='img',
        processors=[ResizeToFit(width=150, height=500)],
        format='JPEG',
        options={'quality': 100}
    )
    created = models.DateTimeField('Время создания', auto_now_add=True)
    approved = models.BooleanField('Включить\Выключить', default=True)

    class Meta():
        verbose_name = 'Изображение'
        verbose_name_plural = 'Изображения'
        ordering = ['-created']

    def __str__(self):
        return str(self.id)
