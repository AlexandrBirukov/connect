from django.urls import reverse_lazy
from .forms import ImageForm
from .models import Image
from django.views.generic import (
    TemplateView,
    CreateView,
    ListView
)


class MainView(TemplateView):
    '''Тестовое задание'''

    template_name = 'gallery/index.html'

    def get_context_data(self, **kwargs):
        context = super(MainView, self).get_context_data(**kwargs)
        context['page_title'] = self.__doc__
        return context


class ContactView(TemplateView):
    '''Мои контакты'''

    template_name = 'gallery/contacts.html'

    def get_context_data(self, **kwargs):
        context = super(ContactView, self).get_context_data(**kwargs)
        context['page_title'] = self.__doc__
        return context


class ImageCreateView(CreateView):
    '''Форма загрузки изображений'''

    template_name = 'gallery/form.html'
    model = Image
    form_class = ImageForm

    def get_context_data(self, **kwargs):
        context = super(ImageCreateView, self).get_context_data(**kwargs)
        context['page_title'] = self.__doc__
        return context

    def get_success_url(self, **kwargs):
        return reverse_lazy('gallery:gallery')


class ImageListView(ListView):
    '''Галерея изображений'''

    model = Image
    template_name = 'gallery/list.html'

    def get_context_data(self, **kwargs):
        context = super(ImageListView, self).get_context_data(**kwargs)
        context['page_title'] = self.__doc__
        return context

    def get_queryset(self):
        return Image.objects.filter(approved=True)
