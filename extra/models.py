from django.db import models


class DownCode(models.Model):
    '''Код перед </body>'''

    class Meta():
        verbose_name = 'Дополнительный код'
        verbose_name_plural = 'Дополнительный код'

    title = models.CharField('Название', max_length=300)
    code = models.TextField('Код',
                            help_text='Код для вставки на каждую страницу сайта (код счетчиков, метрик и тд). Располагается перед </body>. Будте осторожны! Этот код вставляется как есть - он может повредить работоспособности сайта!')
    approved = models.BooleanField('Включить\Выключить', default=True)

    def __str__(self):
        return self.title


class MetaTag(models.Model):
    '''Мета теги для head'''

    class Meta():
        verbose_name = 'Мета тег для head'
        verbose_name_plural = "Мета теги для head"

    name = models.CharField(
        'name/property',
        max_length=100,
        help_text='Пример содержания: name="keywords"'
    )
    content = models.TextField('Content', )
    approved = models.BooleanField('Включить\Выключить', default=True)

    def __str__(self):
        return self.name
