from django.contrib import admin
from .models import DownCode, MetaTag


class DownCodeAdmin(admin.ModelAdmin):
    list_display = ('title', 'approved')
    actions = ['on_object', 'off_object']

    def on_object(self, request, queryset):
        '''Включаем отображение на сайте'''

        queryset.update(approved=True)
        self.message_user(request, 'Выбранные объекты успешно опубликованы!')

    on_object.short_description = 'Включить на сайте'

    def off_object(self, request, queryset):
        '''Выключаем отображение на сайте'''

        queryset.update(approved=False)
        self.message_user(request, 'Выбранные объекты больше не отображаются на сайте!')

    off_object.short_description = 'Выключить на сайте'


class MetaTagAdmin(admin.ModelAdmin):
    list_display = ('name', 'approved')
    actions = ['on_object', 'off_object']

    def on_object(self, request, queryset):
        '''Включаем отображение на сайте'''

        queryset.update(approved=True)
        self.message_user(request, 'Выбранные объекты успешно опубликованы!')

    on_object.short_description = 'Включить на сайте'

    def off_object(self, request, queryset):
        '''Выключаем отображение на сайте'''

        queryset.update(approved=False)
        self.message_user(request, 'Выбранные объекты больше не отображаются на сайте!')

    off_object.short_description = 'Выключить на сайте'


admin.site.register(DownCode, DownCodeAdmin)
admin.site.register(MetaTag, MetaTagAdmin)
