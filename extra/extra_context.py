from .models import DownCode, MetaTag


def extras(request):
    return {
        'meta_tags': MetaTag.objects.filter(approved=True),
        'codes': DownCode.objects.filter(approved=True),
    }
