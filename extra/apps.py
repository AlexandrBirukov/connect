from django.apps import AppConfig


class ExtraConfig(AppConfig):
    name = 'extra'
    verbose_name = '2. Вспомогательные настройки'
